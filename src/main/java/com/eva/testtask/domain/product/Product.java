package com.eva.testtask.domain.product;

import javax.persistence.Entity;
import com.eva.testtask.domain.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = true)
public class Product extends AbstractEntity {

  private String name;
  private String description;
}
