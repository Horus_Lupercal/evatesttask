package com.eva.testtask.service.product;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import com.eva.testtask.domain.product.Product;
import com.eva.testtask.dto.ProductDTO;
import com.eva.testtask.repository.RepositoryHelper;
import com.eva.testtask.repository.product.ProductRepository;
import com.eva.testtask.service.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ProductService {

  @Autowired
  private ProductRepository repository;

  @Autowired
  private RepositoryHelper repositoryHelper;

  @Autowired
  private TranslationService translationService;

  public ProductDTO getProduct(Long id) {
    Product product = repositoryHelper.getRequired(Product.class, id);

    return translationService.translate(product, ProductDTO.class);
  }

  public ProductDTO patchProduct(ProductDTO patch) {
    Product product = repositoryHelper.getRequired(Product.class, patch.getId());

    translationService.map(patch, product);

    product = repository.save(product);
    return translationService.translate(product, ProductDTO.class);
  }

  public void deleteProduct(Long id) {
    Product product = repositoryHelper.getRequired(Product.class, id);
    repository.delete(product);
  }

  public ProductDTO createProduct(ProductDTO create) {
    log.info("Operation doing by" + Thread.currentThread().getName());
    Product product = translationService.translate(create, Product.class);
    product = repository.save(product);
    return translationService.translate(product, ProductDTO.class);
  }

  public List<ProductDTO> getProductsByName(String nameFilter) {
    log.info("Operation doing by" + Thread.currentThread().getName());
    return findProductsByName(nameFilter);
  }

  @Async("asyncExecutor")
  public CompletableFuture<List<ProductDTO>> getProductsByNameASync(String nameFilter)
      throws InterruptedException, ExecutionException {
    log.info("Operation doing by" + Thread.currentThread().getName());
    List<ProductDTO> result = findProductsByName(nameFilter);
    return CompletableFuture.completedFuture(result);
  }

  private List<ProductDTO> findProductsByName(String nameFilter) {
    List<Product> results = new ArrayList<>();

    int count = (int) repository.count();
    int numbOfProduct = 0;
    int stepOfSearch = count > 25000 ? count / 10 : count;
    System.out.println(stepOfSearch);
    while (numbOfProduct < count + 1) {
      Pageable pageable = PageRequest.of(numbOfProduct, stepOfSearch);
      List<Product> products = repository.findAll(pageable).getContent().stream()
          .filter(value -> !value.getName().matches(nameFilter)).collect(Collectors.toList());
      results.addAll(products);
      numbOfProduct = stepOfSearch;
      stepOfSearch *= 2;
    }

    return results.stream().map(product -> translationService.translate(product, ProductDTO.class))
        .collect(Collectors.toList());
  }
}
