package com.eva.testtask.service;

import com.eva.testtask.domain.product.Product;
import com.eva.testtask.dto.ProductDTO;
import org.bitbucket.brunneng.ot.Configuration;
import org.bitbucket.brunneng.ot.ObjectTranslator;
import org.bitbucket.brunneng.ot.exceptions.TranslationException;
import org.springframework.stereotype.Service;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class TranslationService {

  private ObjectTranslator objectTranslator;

  public TranslationService() {
    objectTranslator = new ObjectTranslator(createConfiguration());
  }

  public <T> T translate(Object srcObject, Class<T> targetClass) {
    try {
      return objectTranslator.translate(srcObject, targetClass);
    } catch (TranslationException e) {
      log.warn(e.getMessage());
      throw (RuntimeException) e.getCause();
    }
  }

  public void map(Object srcObject, Object destObject) {
    try {
      objectTranslator.mapBean(srcObject, destObject);
    } catch (TranslationException e) {
      log.warn(e.getMessage());
      throw (RuntimeException) e.getCause();
    }
  }

  private Configuration createConfiguration() {
    Configuration c = new Configuration();
    configureForProduct(c);
    return c;
  }

  private void configureForProduct(Configuration c) {
    c.beanOfClass(ProductDTO.class).translationTo(Product.class).mapOnlyNotNullProperties();
  }
}
