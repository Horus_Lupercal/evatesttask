package com.eva.testtask.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import com.eva.testtask.exception.EntityNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class RepositoryHelper {

  @PersistenceContext
  private EntityManager entityManager;

  public <E> E getReference(Class<E> entityClass, Long id) {
    validateExists(entityClass, id);
    return entityManager.getReference(entityClass, id);
  }

  public <E> E getRequired(Class<E> entityClass, Long id) {
    validateExists(entityClass, id);
    return entityManager.find(entityClass, id);
  }

  private <E> void validateExists(Class<E> entityClass, Long id) {
    Query query = entityManager
        .createQuery("select count(e) from " + entityClass.getSimpleName() + " e where e.id = :id");
    query.setParameter("id", id);
    boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
    if (!exists) {
      throw new EntityNotFoundException(entityClass, id);
    }
  }
}
