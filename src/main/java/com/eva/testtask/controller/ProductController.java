package com.eva.testtask.controller;

import java.util.List;
import java.util.concurrent.ExecutionException;
import com.eva.testtask.dto.ProductDTO;
import com.eva.testtask.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shop/products")
public class ProductController {

  @Autowired
  private ProductService service;

  @GetMapping("/{id}")
  public ProductDTO getProduct(@PathVariable Long id) {
    return service.getProduct(id);
  }

  @PatchMapping
  public ProductDTO patchProduct(@RequestBody ProductDTO patch) {
    return service.patchProduct(patch);
  }

  @PostMapping
  public ProductDTO createProduct(@RequestBody ProductDTO create) {
    return service.createProduct(create);
  }

  @DeleteMapping("/{id}")
  public void deleteProduct(@PathVariable Long id) {
    service.deleteProduct(id);
  }

  @GetMapping
  public List<ProductDTO> getProductsByName(String nameFilter) {
    return service.getProductsByName(nameFilter);
  }

  @GetMapping("/async")
  public List<ProductDTO> getProductsByNameAsync(String nameFilter)
      throws InterruptedException, ExecutionException {
    List<ProductDTO> responce = service.getProductsByNameASync(nameFilter).get();
    return responce;
  }
}
