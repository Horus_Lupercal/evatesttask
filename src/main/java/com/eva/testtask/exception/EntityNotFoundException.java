package com.eva.testtask.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {
  public EntityNotFoundException(Class entityClass, Long id) {
    super(String.format("Entity %s with id=%d is not found!", entityClass.getSimpleName(), id));
  }
}
