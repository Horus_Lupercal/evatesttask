package com.eva.testtask.exception.handler;

import org.springframework.http.HttpStatus;
import lombok.Getter;

@Getter
public class ErrorInfo {
  private final HttpStatus status;
  private final Class exceptionClass;
  private final String message;

  public ErrorInfo(HttpStatus status, Class exceptionClass, String message) {
    this.status = status;
    this.exceptionClass = exceptionClass;
    this.message = message;
  }
}
