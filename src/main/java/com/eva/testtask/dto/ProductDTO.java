package com.eva.testtask.dto;

import java.time.Instant;
import lombok.Data;

@Data
public class ProductDTO {

  private Long id;
  private String name;
  private String description;
  private Instant createdAt;
  private Instant updatedAt;
}
