package com.eva.testtask.service;

import java.util.List;
import java.util.concurrent.ExecutionException;
import com.eva.testtask.BaseTest;
import com.eva.testtask.domain.product.Product;
import com.eva.testtask.dto.ProductDTO;
import com.eva.testtask.exception.EntityNotFoundException;
import com.eva.testtask.repository.product.ProductRepository;
import com.eva.testtask.service.product.ProductService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductServiceTest extends BaseTest {

  @Autowired
  private ProductRepository repository;

  @Autowired
  private ProductService service;

  @Test
  public void testGetProductExpectedEntity() {
    Product product = createProduct();

    ProductDTO responce = service.getProduct(product.getId());

    Assertions.assertThat(responce).isEqualToComparingFieldByField(product);
  }

  @Test
  public void testCreateProductExcpectedSameEntity()
      throws InterruptedException, ExecutionException {
    ProductDTO create = generateEntity(ProductDTO.class);

    ProductDTO afterCreate = service.createProduct(create);

    Assertions.assertThat(afterCreate).isEqualToIgnoringGivenFields(create, "id", "updatedAt",
        "createdAt");
    Assert.assertNotNull(afterCreate.getId());

    ProductDTO product = service.getProduct(afterCreate.getId());

    Assertions.assertThat(product).isEqualToComparingFieldByField(afterCreate);
  }

  @Test
  public void testDeleteProductExcpectedEntityDeleted() {
    Product product = createProduct();

    service.deleteProduct(product.getId());
    Assert.assertFalse(repository.existsById(product.getId()));
  }

  @Test
  public void testPatchProductExcpectedSameEntity() {
    Product product = createProduct();

    ProductDTO patch = generateEntity(ProductDTO.class);
    patch.setId(product.getId());

    ProductDTO afterPatch = service.patchProduct(patch);
    Assertions.assertThat(afterPatch).isEqualToIgnoringGivenFields(patch, "updatedAt", "createdAt");

    product = repository.findById(product.getId()).get();
    Assertions.assertThat(product).isEqualToIgnoringGivenFields(afterPatch, "updatedAt",
        "createdAt");
  }

  @Test
  public void testPatchProductWithEmptyPatch() throws Exception {
    Product product = createProduct();

    ProductDTO patch = new ProductDTO();
    patch.setId(product.getId());

    ProductDTO afterPatch = service.patchProduct(patch);

    Assertions.assertThat(afterPatch).isEqualToIgnoringGivenFields(product, "updatedAt",
        "createdAt");
  }

  @Test(expected = EntityNotFoundException.class)
  public void testGetProductByWrongIdExpectedException()
      throws InterruptedException, ExecutionException {
    Long id = (long) (Math.random() * 1_000_0000);
    service.getProduct(id);
  }

  @Test(expected = EntityNotFoundException.class)
  public void testDeleteProductByWrongIdExpectedException() {
    Long id = (long) (Math.random() * 1_000_0000);
    service.deleteProduct(id);
  }

  @Test
  public void testFindProductByNameFilterExcpectedEntities() {
    Product firstProduct = new Product();
    firstProduct.setName("eva");
    firstProduct.setDescription("dddd");
    firstProduct = repository.save(firstProduct);

    Product secondProduct = new Product();
    secondProduct.setName("ggg");
    secondProduct.setDescription("ssss");
    secondProduct = repository.save(secondProduct);

    String regex = "^.*[eva].*$";

    List<ProductDTO> result = service.getProductsByName(regex);

    Assert.assertTrue(result.size() == 1);
    Assertions.assertThat(secondProduct).isEqualToIgnoringGivenFields(result.get(0));
  }

  @Test
  public void testFindProductByNameFilterAcyncExcpectedEntities()
      throws InterruptedException, ExecutionException {
    Product firstProduct = new Product();
    firstProduct.setName("eva");
    firstProduct.setDescription("dddd");
    firstProduct = repository.save(firstProduct);

    Product secondProduct = new Product();
    secondProduct.setName("ggg");
    secondProduct.setDescription("ssss");
    secondProduct = repository.save(secondProduct);

    Thread.sleep(1000);

    String regex = "^.*[eva].*$";

    List<ProductDTO> products = service.getProductsByNameASync(regex).get();

    Assert.assertTrue(products.size() == 1);
    Assertions.assertThat(secondProduct).isEqualToIgnoringGivenFields(products.get(0));
  }

  private Product createProduct() {
    Product product = genereteEntityWithoutId(Product.class);
    return repository.save(product);
  }
}
