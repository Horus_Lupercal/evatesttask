package com.eva.testtask.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.time.Instant;
import com.eva.testtask.BaseTest;
import com.eva.testtask.domain.product.Product;
import com.eva.testtask.repository.product.ProductRepository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductRepositoryTest extends BaseTest {

  @Autowired
  private ProductRepository repository;

  @Test
  public void testSaveProductExcpectedSameEntity() {
    Product product = genereteEntityWithoutId(Product.class);

    product = repository.save(product);

    assertNotNull(product.getId());
    assertTrue(repository.findById(product.getId()).isPresent());
  }

  @Test
  public void testCreatedAtIsSet() throws Exception {
    Product product = genereteEntityWithoutId(Product.class);

    product = repository.save(product);

    Instant createdAtBeforeReload = product.getCreatedAt();

    Assert.assertNotNull(createdAtBeforeReload);

    product = repository.findById(product.getId()).get();

    Instant createdAtAfterReload = product.getCreatedAt();

    Assert.assertNotNull(createdAtAfterReload);
    Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
  }

  @Test
  public void testModifiedAtIsSet() throws Exception {
    Product product = genereteEntityWithoutId(Product.class);

    product = repository.save(product);

    Instant createdAtBeforeReload = product.getUpdatedAt();
    Assert.assertNotNull(createdAtBeforeReload);

    product = repository.findById(product.getId()).get();

    Instant createdAtAfterReload = product.getUpdatedAt();
    Assert.assertNotNull(createdAtAfterReload);

    product.setName("test");
    product = repository.save(product);

    product = repository.findById(product.getId()).get();
    Instant createdAtAfterChanges = product.getUpdatedAt();
    Assert.assertNotEquals(createdAtAfterReload, createdAtAfterChanges);
  }

}
