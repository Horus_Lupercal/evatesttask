package com.eva.testtask;

import java.util.ArrayList;
import java.util.List;
import com.eva.testtask.domain.AbstractEntity;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = "delete from product", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public abstract class BaseTest {

  private RandomObjectGenerator generator = new RandomObjectGenerator();

  protected <T extends AbstractEntity> T genereteEntityWithoutId(Class<T> entityClass) {
    T entity = generator.generateRandomObject(entityClass);
    entity.setId(null);
    return entity;
  }

  protected <T> T generateEntity(Class<T> objectClass) {
    return generator.generateRandomObject(objectClass);
  }

  protected <T extends AbstractEntity> List<T> generateListOfEntities(Class<T> entityClass,
      int count) {
    List<T> result = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      T entity = generator.generateRandomObject(entityClass);
      result.add(entity);
    }
    return result;
  }
}
