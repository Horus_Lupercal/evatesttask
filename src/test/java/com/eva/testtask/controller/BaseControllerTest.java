package com.eva.testtask.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
public abstract class BaseControllerTest {

  @Autowired
  protected MockMvc mvc;

  @Autowired
  protected ObjectMapper objectMapper;

  private RandomObjectGenerator generator = new RandomObjectGenerator();

  protected <T> T generateEntity(Class<T> objectClass) {
    return generator.generateRandomObject(objectClass);
  }
}
