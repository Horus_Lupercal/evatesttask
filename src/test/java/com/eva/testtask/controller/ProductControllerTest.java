package com.eva.testtask.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import com.eva.testtask.domain.product.Product;
import com.eva.testtask.dto.ProductDTO;
import com.eva.testtask.exception.EntityNotFoundException;
import com.eva.testtask.exception.handler.ErrorInfo;
import com.eva.testtask.service.product.ProductService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@WebMvcTest(controllers = ProductController.class)
public class ProductControllerTest extends BaseControllerTest {

  @MockBean
  private ProductService productService;

  @Test
  public void testGetProductExpectedSameEntity() throws Exception {
    ProductDTO read = createProduct();

    Mockito.when(productService.getProduct(read.getId())).thenReturn(read);

    String resultJson = mvc.perform(get("/shop/products/{id}", read.getId()))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    ProductDTO actualProduct = objectMapper.readValue(resultJson, ProductDTO.class);
    Assertions.assertThat(actualProduct).isEqualToComparingFieldByField(read);

    Mockito.verify(productService).getProduct(read.getId());
  }

  @Test
  public void testPatchProductExcpectedSameEntity() throws Exception {
    ProductDTO patch = createProduct();

    Mockito.when(productService.patchProduct(patch)).thenReturn(patch);

    String resultJson = mvc
        .perform(patch("/shop/products").content(objectMapper.writeValueAsString(patch))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    ProductDTO actualProduct = objectMapper.readValue(resultJson, ProductDTO.class);
    Assertions.assertThat(actualProduct).isEqualToComparingFieldByField(patch);
  }

  @Test
  public void testCreateProductExcpectedSameEntity() throws Exception {
    ProductDTO create = createProduct();

    Mockito.when(productService.createProduct(create)).thenReturn(create);

    String resultJson = mvc
        .perform(post("/shop/products").content(objectMapper.writeValueAsString(create))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    ProductDTO actualProduct = objectMapper.readValue(resultJson, ProductDTO.class);
    Assertions.assertThat(actualProduct).isEqualToComparingFieldByField(create);
  }

  @Test
  public void testDeleteProductExcpectedEntityDeleted() throws Exception {
    Long id = (long) (Math.random() * 1_000_0000);

    mvc.perform(delete("/shop/products/{id}", id)).andExpect(status().isOk());

    Mockito.verify(productService).deleteProduct(id);
  }

  @Test
  public void testGetProductByWrongIdExcpectedException() throws Exception {
    Long wrongId = (long) (Math.random() * 1_000_0000);

    EntityNotFoundException exception = new EntityNotFoundException(Product.class, wrongId);
    Mockito.when(productService.getProduct(wrongId)).thenThrow(exception);

    String resultJson = mvc.perform(get("/shop/products/{id}", wrongId))
        .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

    Assert.assertTrue(resultJson.contains(exception.getMessage()));
  }

  @Test
  public void testNotCorrectIdFormatExcpectedException() throws Exception {
    String wrongID = "ads";
    ErrorInfo exceptionError =
        new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class,
            "Failed to convert value of type 'java.lang.String' to required type 'java.lang.Long'; "
                + "nested exception is java.lang.NumberFormatException: For input string: \""
                + wrongID + "\"");

    String resultJson = mvc.perform(get("/shop/products/{id}", wrongID))
        .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

    ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);

    Assertions.assertThat(actualError).isEqualToComparingFieldByField(exceptionError);
  }

  private ProductDTO createProduct() {
    ProductDTO read = generateEntity(ProductDTO.class);
    return read;
  }

  @Test
  public void testGetProductByNameExcpectedEntity() throws Exception {
    List<ProductDTO> list = Arrays.asList(createProduct());

    Mockito.when(productService.getProductsByName("^.*[1-8].*$")).thenReturn(list);

    String resultJson = mvc.perform(get("/shop/products").param("nameFilter", "^.*[1-8].*$"))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    List<ProductDTO> actualList =
        objectMapper.readValue(resultJson, new TypeReference<List<ProductDTO>>() {});
    Assert.assertEquals(list, actualList);
  }

  @Test
  public void testGetProductByNameAcyncExcpectedEntites() throws Exception {
    CompletableFuture<List<ProductDTO>> list =
        CompletableFuture.completedFuture(Arrays.asList(createProduct()));

    Mockito.when(productService.getProductsByNameASync("^.*[1-8].*$")).thenReturn(list);

    String resultJson = mvc.perform(get("/shop/products/async").param("nameFilter", "^.*[1-8].*$"))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    List<ProductDTO> actualList =
        objectMapper.readValue(resultJson, new TypeReference<List<ProductDTO>>() {});
    Assert.assertEquals(list.get(), actualList);
  }
}
